# Installation Steps:

1. Purchase pforum from https://pforum.org/buy/
2. Go to your customer area, go to your avatar in the top-right corner and select "My Licenses"
3. Download the pforum.zip file and extract it to your server.

Once you have it extracted, follow the steps:

When you first extract pforum and go to your website, it will show an introduction screen. Select "Continue to Installation".

MySQL should be your host's SQL credentials, if you do not know what these are, contact your host.
Once you fill in the MySQL credentials, it will ask for your 25-digit license key. Paste it in (it will send you an email containing it when you buy it). and click "Start Installation".

!!DO NOT REFRESH THE PAGE!! Refreshing during the installation portion can cause corruption.

After the automated installation is over, it will ask you to type your admin account credentials. Enter whatever you want, and then click "Start using pforum". It will redirect you to your admin
panel. Installation is complete!