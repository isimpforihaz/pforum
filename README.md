Welcome to pforum!


pforum is a privacy-aimed forum software.


Features that we like:


- Custom themes! You can build your own theme directly from your website's admin panel. Or you can go to https://pforum.org/themes/ to get a premade forums theme!
- Active development! We have developers worldwide working around the clock to make sure pforum is just right!
- Fast support! We have over 20 support team members worldwide to make sure your forum is working correctly, just the way you want it.
- IP-bans and punishments! Is a user misbehaving? Well now you don't have to deal with them anymore!
- Forum anti-spam bot and other anti-raid features! Now you don't have to worry about annoying spam bots cluttering your forum up.
- Easy to install!